<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Formulir P2H</title>
    <style>
        *{
            font-size: 10px !important;
        }
        td, th{
            padding: 0px !important;
        }
        #table-checklist td, #table-checklist th{
            border: 1px solid #000 !important;
        }
        .list-type-upper-alpha
        {
            list-style-position: inside !important;
            padding-left: 5px !important
        }
        td, th{
            word-wrap: break-word;
        }
        .text-value{
            word-break: break-all;
        }
    </style>
  </head>
  <body>
    <div class="p-0">
        <div class="d-flex justify-content-between flex-md-row flex-column pb-2">
            <div>
                <div class="d-flex mb-0">
                    <h1 class="text-center" style="font-size: 12px !important;">FORM PELAKSANAAN PERAWATAN HARIAN ( P2H )</h1>
                </div>
            </div>
        </div>

        <div>
            <table class="table table-borderless mb-2">
                <tr>
                    <td class="text-nowrap" style="width:1%; padding-left: 0px !important;">Nomor Dokumen</td>
                    <td style="width: 5%; text-align: center">:</td>
                    <td>{{ $p2h->nomor_dokumen }}</td>
                </tr>
                <tr>
                    <td style="width:1%; padding-left: 0px !important;">Unit</td>
                    <td style="width: 5%; text-align: center">:</td>
                    <td>{{ $p2h->unit->no_unit }}</td>
                </tr>
                <tr>
                    <td style="width:1%; padding-left: 0px !important;">Jam</td>
                    <td style="width: 5%; text-align: center">:</td>
                    <td>{{ $p2h->jam }}</td>
                </tr>
                <tr>
                    <td style="width:1%; padding-left: 0px !important;">Hm / Km</td>
                    <td style="width: 5%; text-align: center">:</td>
                    <td>{{ $p2h->hm_km }}</td>
                </tr>
            </table>
            <table class="table table-bordered w-100" style="table-layout:fixed;" id="table-checklist">
                <tr>
                    <th style="width: 5%; padding-left: 5px !important;">No</th>
                    <th class="text-center px-1" style="width: 5%">PIC</th>
                    <th style="padding-left: 5px !important;">BAGIAN YANG HARUS DIPERIKSA</th>
                    <th class="text-center" style="width: 8%">KODE<br>BAHAYA</th>
                    <th class="text-center" style="width: 100px">KONDISI</th>
                    <th class="text-center">CATATAN / TEMUAN</th>
                    <th class="text-center">COMMENT / JAWABAN</th>
                </tr>
                @php
                    $indexCheckbox = 0;
                @endphp
                @foreach ($list_kategori as $indexKategori => $kategori)
                    <tr>
                        <th colspan="7"  style="padding: 5px !important">
                            <ol class="list-type-upper-alpha mb-0" start="{{ $indexKategori + 1 }}">
                                <li>{{ $kategori }}</li>
                            </ol>
                        </th>
                    </tr>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($list_pemeriksaan[$kategori] as  $pemeriksaan)
                        <tr>
                            <td class="font-weight-bold text-nowrap text-center">{{ $no }}</td>
                            <td class="text-center px-0 text-nowrap">O/D</td>
                            <td style="padding-left: 5px !important;">{{ $pemeriksaan->nama_pemeriksaan }}</td>
                            <td class="text-center">{{ $pemeriksaan->kode_bahaya }}</td>
                            <td class="text-center">{{ $pemeriksaan->status_checklist }}</td>
                            <td class="text-center text-value">{!! $pemeriksaan->catatan !!}</td>
                            <td class="text-center text-value">{!! $pemeriksaan->comment !!}</td>
                        </tr>
                        @php
                            $indexCheckbox++;
                            $no++;
                        @endphp
                    @endforeach
                @endforeach
                <tr>
                    <td colspan="7" class="align-top" style="padding: 5px !important;">
                        <h5>Catatan:</h5>
                        <div style="white-space: pre">{{ $p2h->catatan }}</div>
                    </td>
                </tr>
                <tr>
                    <th colspan="2" style="padding: 5px;"></th>
                    <th class="text-center">Operator</th>
                    <th class="text-center" colspan="2">Mekanik</th>
                    <th class="text-center">Foreman</th>
                    <th class="text-center">Superintendent</th>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 5px !important;">Nama</td>
                    <td class="text-center">{{ $p2h->operator->nama ?? '' }}</td>
                    <td class="text-center" colspan="2">{{ $p2h->mekanik->nama ?? '' }}</td>
                    <td class="text-center">{{ $p2h->foreman->nama ?? '' }}</td>
                    <td class="text-center">{{ $p2h->superintendent->nama ?? '' }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 5px !important;">NIP</td>
                    <td class="text-center">{{ $p2h->operator->nip ?? '' }}</td>
                    <td class="text-center" colspan="2">{{ $p2h->mekanik->nip ?? '' }}</td>
                    <td class="text-center">{{ $p2h->foreman->nip ?? '' }}</td>
                    <td class="text-center">{{ $p2h->superintendent->nip ?? '' }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 5px !important;">T. Tangan</td>
                    <td class="text-center align-middle" style="padding: 1px !important;">
                        @if ($p2h->operator != null)
                            <img src="{{ public_path() . $p2h->operator->tanda_tangan }}" style="height: 90px; width: auto !important;">
                        @endif
                    </td>
                    <td class="text-center align-middle" colspan="2">
                        @if ($p2h->mekanik != null)
                            <img src="{{ public_path() . $p2h->mekanik->tanda_tangan }}" style="height: 90px; width: auto !important;">
                        @endif
                    </td>
                    <td class="text-center align-middle" style="padding: 1px !important;">
                        @if ($p2h->foreman != null)
                            <img src="{{ public_path() . $p2h->foreman->tanda_tangan }}" style="height: 90px; width: auto !important;">
                        @endif
                    </td>
                    <td class="text-right align-middle" style="padding: 1px !important;">
                        @if ($p2h->superintendent != null)
                            <img src="{{ public_path() . $p2h->superintendent->tanda_tangan }}" style="height: 90px; width: auto !important;">
                        @endif
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
