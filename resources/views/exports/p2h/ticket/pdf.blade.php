<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Ticket</title>
    <style>
        *{
            font-size: 12px;
        }
    </style>
  </head>
  <body>
    <div class="p-0">
        <div class="d-flex justify-content-between flex-md-row flex-column pb-2">
            <div>
                <div class="d-flex mb-1">
                    <h1 class="font-weight-normal">Ticket</h1>
                </div>
            </div>
        </div>

        <div>
            <table class="table table-borderless mb-2">
                <tr>
                    <td class="text-nowrap" style="width:1%; padding-left: 0px !important;">Nomor Dokumen</td>
                    <td class="px-0" style="width: 1%">:</td>
                    <td>{{ $p2h->nomor_dokumen }}</td>
                </tr>
                <tr>
                    <td style="width:1%; padding-left: 0px !important;">Unit</td>
                    <td class="px-0" style="width: 1%">:</td>
                    <td>{{ $p2h->unit->no_unit }}</td>
                </tr>
                <tr>
                    <td style="width:1%; padding-left: 0px !important;">Date</td>
                    <td class="px-0" style="width: 1%">:</td>
                    <td>{{ date('Y-m-d') }}</td>
                </tr>
                <tr>
                    <td class="px-0" colspan="3">Unit tersebut mengalami rusak pada fungsi :</td>
                </tr>
            </table>
            <table class="table m-0">
                <thead>
                    <tr>
                        <th class="py-1 pl-4">BAGIAN PEMERIKSAAN</th>
                        <th class="py-1">KONDISI</th>
                        <th class="py-1">CATATAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list_pemeriksaan_rusak as $pemeriksaan)
                        <tr>
                            <td>{{ $pemeriksaan->nama_pemeriksaan }}</td>
                            <td>{{ $pemeriksaan->status_checklist }}</td>
                            <td>{!! $pemeriksaan->catatan !!}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <table class="table table-borderless mt-2 mb-4">
                <tr>
                    <td class="px-0" colspan="3">Telah dinyatakan unit tersebut dalam keadaan atau kondisi yang Rusak/Tidak Normal oleh {{ $p2h->mekanik->nama }} sebagai Mechanic dan telah disetujui oleh {{ Auth::user()->nama }} selaku Foreman atau Pengawas dari unit yang disebutkan.</td>
                </tr>
                <tr>
                    <td class="font-weight-bold" style="width: 20vw">MECHANIC,</td>
                    <td></td>
                    <td class="font-weight-bold"  style="width: 1%">FOREMAN/PENGAWAS,</td>
                </tr>
                <tr>
                    <td style="width: 20vw"><img src="{{ public_path() . $p2h->mekanik->tanda_tangan }}" style="height: 100px"></td>
                    <td></td>
                    <td style="width: 1%"><img src="{{ public_path() . $p2h->foreman->tanda_tangan }}" style="height: 100px"></td>
                </tr>
                <tr>
                    <td style="width: 20vw"><u>{{ $p2h->mekanik->nama }}</u></td>
                    <td></td>
                    <td style="width: 1%"><u>{{ $p2h->foreman->nama }}</u></td>
                </tr>
                <tr>
                    <td style="width: 20vw">{{ $p2h->mekanik->nip }}</td>
                    <td></td>
                    <td style="width: 1%">{{ $p2h->foreman->nip }}</td>
                </tr>
            </table>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
