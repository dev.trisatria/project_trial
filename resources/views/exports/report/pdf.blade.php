<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Report P2H</title>
    <style>
        *{
            font-size: 12px;
        }
    </style>
  </head>
  <body>
    <div class="p-0">
        <div class="d-flex justify-content-between flex-md-row flex-column pb-2">
            <div>
                <div class="d-flex mb-1">
                    <h1 class="font-weight-normal">Report</h1>
                </div>
            </div>
        </div>

        <div>
            <table class="table table-bordered" id="table-report">
                <thead>
                    <tr>
                        <th class="text-center align-middle" rowspan="2">Unit List</th>
                        <th class="text-center" rowspan="1" colspan="5">P2H</th>
                    </tr>
                    <tr>
                        <th class="text-center">No. Doc</th>
                        <th class="text-center">Operator</th>
                        <th class="text-center">Mekanik</th>
                        <th class="text-center">Foreman</th>
                        <th class="text-center">Superintendent</th>
                    </tr>
                </thead>
                <tbody id="tbody-report">
                    @foreach ($list_report as $key => $report)
                        <tr>
                            <td class="align-middle text-center">{{ $report->no_unit }}</td>
                            <td class="align-middle">
                                <a href="{{ route('dashboard.p2h.show', $report->firebase_id) }}">{{ $report->nomor_dokumen }}</a>
                                <br>
                                <p class="mb-0">{{ $report->status }}</p>
                            </td>
                            <td style="text-align: center;">{{ $report->date_approve_operator != 'out' ? date('d/m/Y H:i', strtotime($report->date_approve_operator)) : 'out' }}</td>
                            <td style="text-align: center;">{{ $report->date_approve_mekanik != 'out' ? date('d/m/Y H:i', strtotime($report->date_approve_mekanik)) : 'out' }}</td>
                            <td style="text-align: center;">{{ $report->date_approve_foreman != 'out' ? date('d/m/Y H:i', strtotime($report->date_approve_foreman)) : 'out' }}</td>
                            <td style="text-align: center;">{{ $report->date_approve_superintendent != 'out' ? date('d/m/Y H:i', strtotime($report->date_approve_superintendent)) : 'out' }}</td>
                                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
