@extends('layouts.layout2')

@section('title', 'Beranda')

@push('script')
    <script>
        $("a[href='{{ route('dashboard.index') }}']").closest(".nav-item").addClass("active");
    </script>
@endpush
