@extends('layouts.layout2')

@section('title', 'P2H')

@push('style')
    <style>
        .list-type-upper-alpha
        {
            list-style-type: upper-alpha !important;
        }
        td, th{
            word-wrap: break-word;
        }
        .text-value{
            word-break: break-all;
        }
    </style>
@endpush

@section('content_header')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2 d-md-block d-none">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Detail P2H</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard.index') }}">Beranda</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard.p2h.index') }}">P2H</a>
                            </li>
                            <li class="breadcrumb-item active">Detail P2H</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-right col-md-3 col-12">
            <div class="form-group breadcrumb-right">
                <div class="dropdown">
                    <a href="{{ route('dashboard.p2h.export-pdf', $p2h->firebase_id) }}" class="btn btn-danger btn-round" id="btn-export-pdf">
                        Download PDF
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3>Formulir P2H</h3>
                </div>
                <div class="card-body">
                    <table class="table table-borderless d-block d-lg-table table-responsive mb-2">
                        <tr>
                            <td class="text-nowrap" style="width:1%; padding-left: 0px !important;">Nomor Dokumen</td>
                            <td class="px-1" style="width: 1%">:</td>
                            <td colspan="3">{{ $p2h->nomor_dokumen }}</td>
                        </tr>
                        <tr>
                            <td style="width:1%; padding-left: 0px !important;">Unit</td>
                            <td class="px-1" style="width: 1%">:</td>
                            <td colspan="3">{{ $p2h->unit->no_unit }}</td>
                        </tr>
                        <tr>
                            <td style="width:1%; padding-left: 0px !important;">Jam</td>
                            <td class="px-1" style="width: 1%">:</td>
                            <td colspan="3">{{ $p2h->jam }}</td>
                        </tr>
                        <tr>
                            <td style="width:1%; padding-left: 0px !important;">Hm / Km</td>
                            <td class="px-1" style="width: 1%">:</td>
                            <td colspan="3">{{ $p2h->hm_km }}</td>
                        </tr>
                    </table>
                    <table class="table table-bordered d-block d-lg-table table-responsive">
                        <tr>
                            <th style="width: 1%">No</th>
                            <th class="text-center px-0" style="width: 1%">PIC</th>
                            <th>BAGIAN YANG HARUS DIPERIKSA</th>
                            <th class="text-center" style="width: 1%">KODE<br>BAHAYA</th>
                            <th class="text-center" style="width: 1%">KONDISI</th>
                            <th class="text-center" style="width: 20vw">CATATAN/TEMUAN</th>
                            <th class="text-center" style="width: 20vw">COMMENT/JAWABAN</th>
                        </tr>
                        @php
                            $indexCheckbox = 0;
                        @endphp
                        @foreach ($list_kategori as $indexKategori => $kategori)
                            <tr>
                                <th colspan="7" class="pl-0">
                                    <ol class="list-type-upper-alpha mb-0" start="{{ $indexKategori + 1 }}">
                                        <li>{{ $kategori }}</li>
                                    </ol>
                                </th>
                            </tr>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($list_pemeriksaan[$kategori] as  $pemeriksaan)
                                <tr>
                                    <td class="font-weight-bold">{{ $no }}</td>
                                    <td class="text-center px-0">O/D</td>
                                    <td>{{ $pemeriksaan->nama_pemeriksaan }}</td>
                                    <td class="text-center">{{ $pemeriksaan->kode_bahaya }}</td>
                                    <td class="text-center">{{ $pemeriksaan->status_checklist }}</td>
                                    <td class="text-center text-value">{!! $pemeriksaan->catatan !!}</td>
                                    <td class="text-center text-value">{!! $pemeriksaan->comment !!}</td>
                                </tr>
                                @php
                                    $indexCheckbox++;
                                    $no++;
                                @endphp
                            @endforeach
                        @endforeach
                        <tr>
                            <td colspan="7" class="align-top" style="height: 150px;">
                                <h5>Catatan:</h5>
                                <div style="white-space: pre">{{ $p2h->catatan }}</div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2"></th>
                            <th class="text-center">Operator</th>
                            <th class="text-center" colspan="2">Mekanik</th>
                            <th class="text-center">Foreman</th>
                            <th class="text-center">Superintendent</th>
                        </tr>
                        <tr>
                            <td colspan="2">Nama</td>
                            <td class="text-center">{{ $p2h->operator->nama ?? '' }}</td>
                            <td class="text-center" colspan="2">{{ $p2h->mekanik->nama ?? '' }}</td>
                            <td class="text-center">{{ $p2h->foreman->nama ?? '' }}</td>
                            <td class="text-center">{{ $p2h->superintendent->nama ?? '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">NIP</td>
                            <td class="text-center">{{ $p2h->operator->nip ?? '' }}</td>
                            <td class="text-center" colspan="2">{{ $p2h->mekanik->nip ?? '' }}</td>
                            <td class="text-center">{{ $p2h->foreman->nip ?? '' }}</td>
                            <td class="text-center">{{ $p2h->superintendent->nip ?? '' }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">T. Tangan</td>
                            <td class="text-center align-middle">
                                @if ($p2h->operator != null)
                                    <img src="{{ $p2h->operator->tanda_tangan }}" style="height: 100px; width: auto !important;">
                                @endif
                            </td>
                            <td class="text-center align-middle" colspan="2">
                                @if ($p2h->mekanik != null)
                                    <img src="{{ $p2h->mekanik->tanda_tangan }}" style="height: 100px; width: auto !important;">
                                @endif
                            </td>
                            <td class="text-center align-middle">
                                @if ($p2h->foreman != null)
                                    <img src="{{ $p2h->foreman->tanda_tangan }}" style="height: 100px; width: auto !important;">
                                @endif
                            </td>
                            <td class="text-right align-middle">
                                @if ($p2h->superintendent != null)
                                    <img src="{{ $p2h->superintendent->tanda_tangan }}" style="height: 100px; width: auto !important;">
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

    <script>
        $(document).on("click", "#btn-export-pdf", function()
        {
            startLoading();
        });
        $("a[href='{{ route('dashboard.p2h.index') }}']").closest(".nav-item").addClass("active");
    </script>
@endpush
