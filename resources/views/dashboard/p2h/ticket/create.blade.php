@extends('layouts.layout2')

@section('title', 'Create Ticket P2H')

@push('style')
    <style>
        .list-type-upper-alpha
        {
            list-style-type: upper-alpha !important
        }
        .input-catatan-pemeriksaan, .input-comment-pemeriksaan{
            word-wrap: break-word;
        }
    </style>
@endpush

@section('content_header')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2 d-md-block d-none">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Create Ticket P2H</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard.index') }}">Beranda</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard.p2h.index') }}">P2H</a>
                            </li>
                            <li class="breadcrumb-item active">Create Ticket P2H</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="invoice-print p-3">
                        <div class="d-flex justify-content-between flex-md-row flex-column pb-2">
                            <div>
                                <div class="d-flex mb-1">
                                    <h1 class="font-weight-normal">Ticket</h1>
                                </div>
                            </div>
                        </div>

                        <div>
                            <table class="table table-borderless mb-2">
                                <tr>
                                    <td class="text-nowrap" style="width:1%; padding-left: 0px !important;">Nomor Dokumen</td>
                                    <td class="px-0" style="width: 1%">:</td>
                                    <td>{{ $p2h->nomor_dokumen }}</td>
                                </tr>
                                <tr>
                                    <td style="width:1%; padding-left: 0px !important;">Unit</td>
                                    <td class="px-0" style="width: 1%">:</td>
                                    <td>{{ $p2h->unit->no_unit }}</td>
                                </tr>
                                <tr>
                                    <td style="width:1%; padding-left: 0px !important;">Date</td>
                                    <td class="px-0" style="width: 1%">:</td>
                                    <td>{{ date('Y-m-d') }}</td>
                                </tr>
                                <tr>
                                    <td class="px-0" colspan="3">Unit tersebut mengalami rusak pada fungsi :</td>
                                </tr>
                            </table>
                            <table class="table m-0">
                                <thead>
                                    <tr>
                                        <th class="py-1 pl-4">BAGIAN PEMERIKSAAN</th>
                                        <th class="py-1">KONDISI</th>
                                        <th class="py-1">CATATAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($list_pemeriksaan_rusak as $pemeriksaan)
                                        <tr>
                                            <td>{{ $pemeriksaan->nama_pemeriksaan }}</td>
                                            <td>{{ $pemeriksaan->status_checklist }}</td>
                                            <td>{{ $pemeriksaan->catatan }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <table class="table table-borderless mt-2 mb-4">
                                <tr>
                                    <td class="px-0" colspan="3">Telah dinyatakan unit tersebut dalam keadaan atau kondisi yang Rusak/Tidak Normal oleh {{ $p2h->mekanik->nama }} sebagai Mechanic dan telah disetujui oleh {{ Auth::user()->nama }} selaku Foreman atau Pengawas dari unit yang disebutkan.</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold" style="width: 20vw">MECHANIC,</td>
                                    <td></td>
                                    <td class="font-weight-bold"  style="width: 20vw">FOREMAN/PENGAWAS,</td>
                                </tr>
                                <tr>
                                    <td style="width: 20vw"><img src="{{ $p2h->mekanik->tanda_tangan }}" style="height: 100px"></td>
                                    <td></td>
                                    <td style="width: 20vw"><img src="{{ Auth::user()->tanda_tangan }}" style="height: 100px"></td>
                                </tr>
                                <tr>
                                    <td style="width: 20vw"><u>{{ $p2h->mekanik->nama }}</u></td>
                                    <td></td>
                                    <td style="width: 20vw"><u>{{ Auth::user()->nama }}</u></td>
                                </tr>
                                <tr>
                                    <td style="width: 20vw">{{ $p2h->mekanik->nip }}</td>
                                    <td></td>
                                    <td style="width: 20vw">{{ Auth::user()->nip }}</td>
                                </tr>
                            </table>
                            <form action="{{ route('dashboard.p2h.update', $p2h->firebase_id) }}" method="post" id="form-edit">
                                @csrf
                                @method("PUT")
                                <input type="hidden" name="current_time" class="input-current-time">
                                <input type="hidden" value="{{ $p2h->unit_id }}" id="input-unit-id">
                                <input type="hidden" value="{{ $p2h->unit->no_unit }}" id="input-no-unit">
                                <input type="hidden" value="{{ $p2h->nomor_dokumen }}" id="input-nomor-dokumen">
                                <input type="hidden" value="{{ $p2h->jam }}" id="input-jam">
                                <input type="hidden" value="{{ $p2h->hm_km }}" id="input-hm-km">
                                <input type="hidden" name="foreman_id" value="{{ Auth::user()->id }}" id="input-user-id">
                                <input type="hidden" name="status" value="Ticket Created" id="input-status">

                                <div style="display: none">
                                    <table class="table table-borderless mb-2">
                                        <tr>
                                            <td class="text-nowrap" style="width:1%; padding-left: 0px !important;">Nomor Dokumen</td>
                                            <td class="px-1" style="width: 1%">:</td>
                                            <td colspan="3">{{ $p2h->nomor_dokumen }}</td>
                                        </tr>
                                        <tr>
                                            <td style="width:1%; padding-left: 0px !important;">Unit</td>
                                            <td class="px-1" style="width: 1%">:</td>
                                            <td colspan="3">{{ $p2h->unit->no_unit }}</td>
                                        </tr>
                                        <tr>
                                            <td style="width:1%; padding-left: 0px !important;">Jam</td>
                                            <td class="px-1" style="width: 1%">:</td>
                                            <td colspan="3">{{ $p2h->jam }}</td>
                                        </tr>
                                        <tr>
                                            <td style="width:1%; padding-left: 0px !important;">Hm / Km</td>
                                            <td class="px-1" style="width: 1%">:</td>
                                            <td colspan="3">{{ $p2h->hm_km }}</td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered table-responsive d-block d-md-table">
                                        <tr>
                                            <th style="width: 1%">No</th>
                                            <th class="text-center px-0" style="width: 1%">PIC</th>
                                            <th>BAGIAN YANG HARUS DIPERIKSA</th>
                                            <th class="text-center" style="width: 1%">KODE<br>BAHAYA</th>
                                            <th class="text-center" style="width: 1%">KONDISI</th>
                                            <th class="text-center" style="width: 310px">CATATAN/TEMUAN</th>
                                            <th class="text-center" style="width: 310px">COMMENT/JAWABAN</th>
                                        </tr>
                                        @php
                                            $indexInput = 0;
                                        @endphp
                                        @foreach ($list_kategori as $indexKategori => $kategori)
                                            <tr>
                                                <th colspan="7" class="pl-0">
                                                    <ol class="list-type-upper-alpha mb-0" start="{{ $indexKategori + 1 }}">
                                                        <li>{{ $kategori }}</li>
                                                    </ol>
                                                </th>
                                            </tr>
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach ($list_pemeriksaan[$kategori] as  $pemeriksaan)
                                                <tr>
                                                    <td class="font-weight-bold">{{ $no }}</td>
                                                    <td class="text-center px-0">O/D</td>
                                                    <td>{{ $pemeriksaan->nama_pemeriksaan }}</td>
                                                    <td class="text-center">{{ $pemeriksaan->kode_bahaya }}</td>
                                                    <td class="text-center">
                                                        {{ $pemeriksaan->status_checklist }}
                                                        <input type="hidden" name="list_item[{{ $indexInput }}][id]" value="{{ $pemeriksaan->id }}">
                                                        <input type="hidden" name="list_item[{{ $indexInput }}][p2h_id]" value="{{ $p2h->id }}">
                                                        <input type="hidden" name="list_item[{{ $indexInput }}][list_pemeriksaan_id]" value="{{ $pemeriksaan->list_pemeriksaan_id }}">
                                                        <input type="hidden" name="list_item[{{ $indexInput }}][status_checklist]" value="{{ $pemeriksaan->status_checklist }}">
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="h-100 w-100 input-catatan-pemeriksaan" contenteditable="{{ Auth::user()->role == 'mekanik' || Auth::user()->role == 'foreman' ? 'true' : 'false' }}">{{ $pemeriksaan->catatan }}</div>
                                                        <input type="hidden" name="list_item[{{ $indexInput }}][catatan]" class="input-catatan-pemeriksaan-hidden" value="{{ $pemeriksaan->catatan }}">
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="h-100 w-100 input-comment-pemeriksaan" contenteditable="{{ Auth::user()->role == 'foreman' ? 'true' : 'false' }}">{{ $pemeriksaan->comment }}</div>
                                                        <input type="hidden" name="list_item[{{ $indexInput }}][comment]" class="input-comment-pemeriksaan-hidden" value="{{ $pemeriksaan->comment }}">
                                                        <input type="hidden" name="list_item[{{ $indexInput }}][created_at]" value="{{ $pemeriksaan->created_at }}">
                                                        <input type="hidden" name="list_item[{{ $indexInput }}][updated_at]" class="input-current-time" value="{{ $pemeriksaan->updated_at }}">
                                                    </td>
                                                </tr>
                                                @php
                                                    $indexInput++;
                                                    $no++;
                                                @endphp
                                            @endforeach
                                        @endforeach
                                        <tr>
                                            <td colspan="7" class="align-top" style="min-height: 150px">
                                                <h5>Catatan:</h5>
                                                <div class="w-100 px-1" id="input-catatan-p2h" contenteditable="{{ Auth::user()->role == 'foreman' ? 'true' : 'false' }}" style="height: 120px">{{ $p2h->catatan }}</div>
                                                <input type="hidden" name="catatan" id="input-catatan-p2h-hidden" value="{{ $p2h->catatan }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="2"></th>
                                            <th class="text-center">Operator</th>
                                            <th class="text-center" colspan="2">Mekanik</th>
                                            <th class="text-center">Foreman</th>
                                            <th class="text-center">Superintendent</th>
                                        </tr>
                                        <tr>
                                            <td colspan="2">Nama</td>
                                            <td class="text-center">{{ $p2h->operator->nama ?? '' }}</td>
                                            <td class="text-center" colspan="2">{{ $p2h->mekanik->nama ?? '' }}</td>
                                            <td class="text-center">{{ $p2h->foreman->nama ?? '' }}</td>
                                            <td class="text-center">{{ $p2h->superintendent->nama ?? '' }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">NIP</td>
                                            <td class="text-center">{{ $p2h->operator->nip ?? '' }}</td>
                                            <td class="text-center" colspan="2">{{ $p2h->mekanik->nip ?? '' }}</td>
                                            <td class="text-center">{{ $p2h->foreman->nip ?? '' }}</td>
                                            <td class="text-center">{{ $p2h->superintendent->nip ?? '' }}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">T. Tangan</td>
                                            <td class="text-center align-middle">
                                                @if ($p2h->operator != null)
                                                    <img src="{{ $p2h->operator->tanda_tangan }}" style="height: 100px; width: auto !important;">
                                                @endif
                                            </td>
                                            <td class="text-center align-middle" colspan="2">
                                                @if ($p2h->mekanik != null)
                                                    <img src="{{ $p2h->mekanik->tanda_tangan }}" style="height: 100px; width: auto !important;">
                                                @endif
                                            </td>
                                            <td class="text-center align-middle">
                                                @if ($p2h->foreman != null)
                                                    <img src="{{ $p2h->foreman->tanda_tangan }}" style="height: 100px; width: auto !important;">
                                                @endif
                                            </td>
                                            <td class="text-right align-middle">
                                                @if ($p2h->superintendent != null)
                                                    <img src="{{ $p2h->superintendent->tanda_tangan }}" style="height: 100px; width: auto !important;">
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <button type="button" class="btn btn-success" id="btn-submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

    <script type="module">
        import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-app.js";
        import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-analytics.js";
        import { getDatabase, ref, set, onValue, push, update, query, orderByKey, limitToLast } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-database.js";

        // Declare variable from php
        const firebaseId = @json($p2h->firebase_id);
        const userId = @json(Auth::user()->id);
        const userRole = @json(Auth::user()->role);
        const dateApproveOperator = @json($p2h->report->date_approve_operator);
        const dateApproveMekanik = @json($p2h->report->date_approve_mekanik ?? 'out');
        const dateApproveForeman = @json($p2h->report->date_approve_foreman ?? 'out');
        const dateApproveSuperintendent = @json($p2h->report->date_approve_superintendent ?? 'out');

        // Firebase configuration
        const firebaseConfig = {
            apiKey: "AIzaSyD8Fm1tbiwv2F3jVfhCLC3dg9zlY7HceyU",
            authDomain: "trisatria-trial.firebaseapp.com",
            databaseURL: "https://trisatria-trial-default-rtdb.asia-southeast1.firebasedatabase.app",
            projectId: "trisatria-trial",
            storageBucket: "trisatria-trial.appspot.com",
            messagingSenderId: "842562798000",
            appId: "1:842562798000:web:a76f064bd14f4276355675",
            measurementId: "G-XFJ3GNYP50"
        };

        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        const analytics = getAnalytics(app);
        const db = getDatabase();

        $(document).on("click", "#btn-submit", function(e)
        {
            e.preventDefault();
            startLoading();
            const datetimeFormat = "YYYY-MM-DD HH:mm:ss";
            let now = moment();
            let currentTime = moment(now).format(datetimeFormat);
            $(".input-current-time").val(currentTime)

            const p2hRef = ref(db, "p2h/" + firebaseId);
            let data = {
                unit_id: $("#input-unit-id").val(),
                no_unit: $("#input-no-unit").val(),
                nomor_dokumen: $("#input-nomor-dokumen").val(),
                jam: $("#input-jam").val(),
                hm_km: $("#input-hm-km").val(),
                date_approve_operator: dateApproveOperator,
                date_approve_mekanik: userRole == "mekanik" ? $(".input-current-time").val() : dateApproveMekanik,
                date_approve_foreman: userRole == "foreman" ? $(".input-current-time").val() : dateApproveForeman,
                date_approve_superintendent: userRole == "superintendent" ? $(".input-current-time").val() : dateApproveSuperintendent,
                status: $("#input-status").val()
            }
            set(p2hRef, data).then(() => {
                $("#form-edit").submit();
            });

        });

        $(".input-date-time").flatpickr({
            enableTime: true
        });
        $("a[href='{{ route('dashboard.p2h.index') }}']").closest(".nav-item").addClass("active");
    </script>
@endpush
