@extends('layouts.layout2')

@section('title', 'Tambah P2H')

@push('style')
    <style>
        .list-type-upper-alpha
        {
            list-style-type: upper-alpha !important;
        }
    </style>
@endpush

@section('content_header')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2 d-md-block d-none">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Tambah P2H</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard.index') }}">Beranda</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard.unit.index') }}">Unit</a>
                            </li>
                            <li class="breadcrumb-item active">Tambah P2H</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3>Formulir P2H</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('dashboard.p2h.store') }}" method="post" id="form-create">
                        @csrf
                        <input type="hidden" name="unit_id" value="{{ request()->unit_id }}" id="input-unit-id">
                        <input type="hidden" name="firebase_id" id="input-firebase-id">
                        <input type="hidden" name="current_time" id="input-current-time">
                        <input type="hidden" name="nomor_dokumen" id="input-nomor-dokumen">
                        <div class="row form-group">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Unit</label>
                                    <input type="text" class="form-control" value="{{ $unit->no_unit }}" readonly id="input-no-unit">
                                </div>
                                <div class="form-group">
                                    <label for="">Jam</label>
                                    <input type="text" name="jam" class="form-control input-date-time" value="{{ date('Y-m-d H:i') }}" id="input-jam" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Hm / Km</label>
                                    <input type="text" name="hm_km" class="form-control" id="input-hm-km" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <table class="table table-bordered table-responsive d-block d-md-table">
                                <thead>
                                    <tr>
                                        <th style="width: 1%">No</th>
                                        <th style="width: 1%">PIC</th>
                                        <th>BAGIAN YANG HARUS DIPERIKSA</th>
                                        <th class="text-center" style="width: 1%">KODE<br>BAHAYA</th>
                                        <th class="text-center" style="width: 1%">KONDISI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $indexCheckbox = 0;
                                    @endphp
                                    @foreach ($list_kategori as $indexKategori => $kategori)
                                        <tr>
                                            <th colspan="5" class="pl-0">
                                                <ol class="list-type-upper-alpha mb-0" start="{{ $indexKategori + 1 }}">
                                                    <li>{{ $kategori }}</li>
                                                </ol>
                                            </th>
                                        </tr>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($list_pemeriksaan[$kategori] as  $pemeriksaan)
                                            <tr>
                                                <td class="font-weight-bold">{{ $no }}</td>
                                                <td class="text-center">O/D</td>
                                                <td class="text-nowrap">{{ $pemeriksaan->nama_pemeriksaan }}</td>
                                                <td class="text-center">{{ $pemeriksaan->kode_bahaya }}</td>
                                                <td class="text-nowrap">
                                                    <input type="hidden" name="list_item[{{ $indexCheckbox }}][list_pemeriksaan_id]" value="{{ $pemeriksaan->id }}">
                                                    <div class="form-check form-check-inline">
                                                        <input type="radio" class="form-check-input" id="status-baik-{{ $indexCheckbox }}" name="list_item[{{ $indexCheckbox }}][status_checklist]" value="Baik/Normal" checked>
                                                        <label class="form-check-label" for="status-baik-{{ $indexCheckbox }}">Baik/Normal</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input type="radio" class="form-check-input" id="status-rusak-{{ $indexCheckbox }}" name="list_item[{{ $indexCheckbox }}][status_checklist]" value="Rusak/Tidak Normal">
                                                        <label class="form-check-label" for="status-rusak-{{ $indexCheckbox }}">Rusak/Tidak Normal</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            @php
                                                $indexCheckbox++;
                                                $no++;
                                            @endphp
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-primary px-3" id="btn-submit" disabled>Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

    <script type="module">
        import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-app.js";
        import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-analytics.js";
        import { getDatabase, ref, set, onValue, push, update, query, orderByKey, limitToLast } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-database.js";

        // Declare variable from php
        const userId = @json(Auth::user()->id);
        const currentDate = @json(date('d-m-Y'));

        // Firebase configuration
        const firebaseConfig = {
            apiKey: "AIzaSyD8Fm1tbiwv2F3jVfhCLC3dg9zlY7HceyU",
            authDomain: "trisatria-trial.firebaseapp.com",
            databaseURL: "https://trisatria-trial-default-rtdb.asia-southeast1.firebasedatabase.app",
            projectId: "trisatria-trial",
            storageBucket: "trisatria-trial.appspot.com",
            messagingSenderId: "842562798000",
            appId: "1:842562798000:web:a76f064bd14f4276355675",
            measurementId: "G-XFJ3GNYP50"
        };

        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        const analytics = getAnalytics(app);
        const db = getDatabase();

        const p2hRef = ref(db, 'p2h');
        const indexState = query(p2hRef, orderByKey(), limitToLast(1));

        let firebaseId = 1;
        onValue(indexState, (snapshot) =>
        {
            if (snapshot.val() != null) {
                firebaseId = parseInt((Object.keys(snapshot.val())[0])) + 1;
            }
        });

        $(document).on("input", "input", function()
        {
            let jam = $("#input-jam").val();
            let hmKm = $("#input-hm-km").val();
            if (jam != "" && hmKm != "")
            {
                $("#btn-submit").prop("disabled", false);
            }else
            {
                $("#btn-submit").prop("disabled", true);
            }
        });

        $(document).on("click", "#btn-submit", function(e)
        {
            e.preventDefault();
            startLoading();
            const datetimeFormat = "YYYY-MM-DD HH:mm:ss";
            let now = moment();
            let currentTime = moment(now).format(datetimeFormat);
            $("#input-current-time").val(currentTime)
            $("#input-firebase-id").val(firebaseId);
            $("#input-nomor-dokumen").val('P2H/' + firebaseId + '/' + currentDate);

            const p2hRef = ref(db, 'p2h/' + firebaseId);
            let data = {
                unit_id: $("#input-unit-id").val(),
                no_unit: $("#input-no-unit").val(),
                nomor_dokumen: $("#input-nomor-dokumen").val(),
                jam: $("#input-jam").val(),
                hm_km: $("#input-hm-km").val(),
                operator_id: userId,
                mekanik_id: 0,
                foreman_id: 0,
                superintendent_id: 0,
                date_approve_operator: currentTime,
                date_approve_mekanik: 'out',
                date_approve_foreman: 'out',
                date_approve_superintendent: 'out',
                status: 'Need Mechanic Verification'
            }
            update(p2hRef, data).then(() => {
                $("#form-create").submit();
            });
        });

        $('.input-date-time').flatpickr({
            enableTime: true
        });
        $("a[href='{{ route('dashboard.p2h.index') }}']").closest(".nav-item").addClass("active");
    </script>
@endpush
