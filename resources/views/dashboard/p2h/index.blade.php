@extends('layouts.layout2')

@section('title', 'P2H')

@push('style')

@endpush

@section('content_header')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2 d-md-block d-none">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">P2H</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard.index') }}">Beranda</a>
                            </li>
                            <li class="breadcrumb-item active">P2H</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <form action="">
                <div class="row mb-2">
                    <div class="col-md-6 mb-1 mb-md-0">
                        <div class="row">
                            <div class="col-6">
                                <input type="date" name="start_date" class="form-control" value="{{ date('Y-m-d', strtotime($start_date)) }}" required>
                            </div>
                            <div class="col-6">
                                <input type="date" name="end_date" class="form-control" value="{{ date('Y-m-d', strtotime($end_date)) }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">Filter</button>
                        <button type="button" class="btn btn-success" id="btn-download-excel">Download Excel</button>
                    </div>
                </div>
            </form>
            <form action="{{ route('dashboard.p2h.export-excel') }}" method="post">
                @csrf
                <textarea name="list_p2h" class="d-none input-p2h">{{ $list_p2h }}</textarea>
                <button type="submit" class="d-none" id="btn-download-excel-hidden">Download Excel</button>
            </form>
            <div class="card">
                <div class="card-body">
                    <table class="table table-responsive d-block d-lg-table" id="table-p2h">
                        <thead>
                            <tr>
                                <th style="width: 1%">ID</th>
                                <th>UNIT</th>
                                <th>Nomor Dokumen</th>
                                <th>Status</th>
                                <th class="text-center" style="width: 1%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($list_p2h as $p2h)
                                <tr>
                                    <td>{{ $p2h->id }}</td>
                                    <td>{{ $p2h->unit->merk_unit }}</td>
                                    <td>{{ $p2h->nomor_dokumen }}</td>
                                    <td>{{ $p2h->status }}</td>
                                    <td>
                                        <div class="btn-group text-nowrap">
                                            <a href="{{ route('dashboard.p2h.show', $p2h->firebase_id) }}" class="btn btn-primary">Detail</a>

                                            @if (Auth::user()->role == 'mekanik' && $p2h->status == 'Need Mechanic Verification')
                                                <a href="{{ route('dashboard.p2h.edit', $p2h->firebase_id) }}" class="btn btn-info">Verifikasi P2H</a>
                                            @endif
                                            @if (Auth::user()->role == 'foreman' && ($p2h->status == 'Need Foreman Verification'))
                                                <a href="{{ route('dashboard.p2h.edit', $p2h->firebase_id) }}" class="btn btn-info">Verifikasi P2H</a>
                                            @endif
                                            @if (Auth::user()->role == 'foreman' && ($p2h->status == 'Need Ticket'))
                                                <a href="{{ route('dashboard.p2h.create-ticket', $p2h->firebase_id) }}" class="btn btn-warning">Create Ticket</a>
                                            @endif
                                            @if (Auth::user()->role == 'superintendent' && $p2h->status == 'Need Superintendent Verification')
                                                <a href="{{ route('dashboard.p2h.edit', $p2h->firebase_id) }}" class="btn btn-info">Verifikasi P2H</a>
                                            @endif
                                            @if ($p2h->status != 'Ticket Created' && $p2h->status != 'Completed')
                                                <a href="javascript:;" class="btn btn-danger btn-reminder" data-link="{{ route('dashboard.p2h.reminder', $p2h->firebase_id) }}">Reminder</a>
                                            @endif
                                            @if ($p2h->status == 'Ticket Created')
                                                <a href="{{ route('dashboard.p2h.export-ticket-pdf', $p2h->firebase_id) }}" class="btn btn-success btn-download-ticket">Download Ticket</a>
                                                <a href="javascript:;" class="btn btn-secondary btn-generate-link" data-link="{{ route('dashboard.p2h.export-ticket-pdf', $p2h->firebase_id) }}">Generate Link Ticket</a>
                                            @endif

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-link">
        <div class="modal-dialog" style="width: 640px; max-width: 100%" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-9 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="input-copy" value="Copy Me!" readonly>
                            </div>
                        </div>
                        <div class="col-sm-3 col-12">
                            <button class="btn btn-outline-primary btn-block waves-effect" id="btn-copy">Copy!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-reminder">
        <div class="modal-dialog" style="width: 640px; max-width: 100%" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Reminder</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" method="post" id="form-reminder">
                        @csrf
                        <p>Tindakan ini akan mengirimkan email notifikasi reminder kepada yang bersangkutan, apakah Anda yakin?</p>
                        <div class="form-group mb-0 text-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

    <script>
        $(document).on("click", "#btn-download-excel", function()
        {
            $("#btn-download-excel-hidden").click();
        });

        $(document).on("click", ".btn-reminder", function()
        {
            let link = $(this).data("link");
            $("#form-reminder").prop("action", link);
            $("#modal-reminder").modal();
        });

        $(document).on("click", ".btn-download-ticket", function()
        {
            startLoading();
        });

        $(document).on("click", ".btn-generate-link", function()
        {
            let link = $(this).data("link");
            $("#input-copy").val(link);
            $("#modal-link").modal();
        });

        $(document).on("submit", "#form-reminder", function()
        {
            $("#modal-reminder").modal('hide');
            startLoading();
        });

        $(document).on("click", "#btn-copy", function () {
            $("#input-copy").select();
            document.execCommand("copy");
            toastr["success"]("", "Copied to clipboard!");
        });

        $("#table-p2h").DataTable();
        $("a[href='{{ route('dashboard.p2h.index') }}']").closest(".nav-item").addClass("active");
    </script>
@endpush
