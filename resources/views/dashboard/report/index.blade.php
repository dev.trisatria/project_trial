@extends('layouts.layout2')

@section('title', 'Report')

@push('style')

@endpush

@section('content_header')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2 d-md-block d-none">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Report</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard.index') }}">Beranda</a>
                            </li>
                            <li class="breadcrumb-item active">Report</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <form action="">
                <div class="row mb-2">
                    <div class="col-md-6 mb-1 mb-md-0">
                        <div class="row">
                            <div class="col-6">
                                <input type="date" name="start_date" class="form-control" value="{{ date('Y-m-d', strtotime($start_date)) }}" required>
                            </div>
                            <div class="col-6">
                                <input type="date" name="end_date" class="form-control" value="{{ date('Y-m-d', strtotime($end_date)) }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">Filter</button>
                        <button type="button" class="btn btn-danger" id="btn-download-pdf">Download PDF</button>
                        <button type="button" class="btn btn-success" id="btn-download-excel">Download Excel</button>
                    </div>
                </div>
            </form>
            <form action="{{ route('dashboard.report.export-pdf') }}" method="post">
                @csrf
                <textarea name="list_report" class="d-none input-report"></textarea>
                <button type="submit" class="d-none" id="btn-download-pdf-hidden">Download PDF</button>
            </form>
            <form action="{{ route('dashboard.report.export-excel') }}" method="post">
                @csrf
                <textarea name="list_report" class="d-none input-report"></textarea>
                <button type="submit" class="d-none" id="btn-download-excel-hidden">Download Excel</button>
            </form>
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-responsive d-block d-lg-table" id="table-report">
                        <thead>
                            <tr>
                                <th class="text-center align-middle" rowspan="2">Unit List</th>
                                <th class="text-center" rowspan="1" colspan="5">P2H</th>
                            </tr>
                            <tr>
                                <th class="text-center">No. Doc</th>
                                <th class="text-center">Operator</th>
                                <th class="text-center">Mekanik</th>
                                <th class="text-center">Foreman</th>
                                <th class="text-center">Superintendent</th>
                            </tr>
                        </thead>
                        <tbody id="tbody-report">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script type="module">
        import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-app.js";
        import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-analytics.js";
        import { getDatabase, ref, set, onValue, push, update, query, orderByChild, startAt, endAt } from "https://www.gstatic.com/firebasejs/9.8.3/firebase-database.js";

        // Declare variable from php
        const startDate = @json($start_date);
        const endDate = @json($end_date);

        // Firebase configuration
        const firebaseConfig = {
            apiKey: "AIzaSyD8Fm1tbiwv2F3jVfhCLC3dg9zlY7HceyU",
            authDomain: "trisatria-trial.firebaseapp.com",
            databaseURL: "https://trisatria-trial-default-rtdb.asia-southeast1.firebasedatabase.app",
            projectId: "trisatria-trial",
            storageBucket: "trisatria-trial.appspot.com",
            messagingSenderId: "842562798000",
            appId: "1:842562798000:web:a76f064bd14f4276355675",
            measurementId: "G-XFJ3GNYP50"
        };

        // Initialize Firebase
        const app = initializeApp(firebaseConfig);
        const analytics = getAnalytics(app);
        const db = getDatabase();

        const p2hRef = ref(db, 'p2h');
        const filterState = query(p2hRef, orderByChild('jam'), startAt(startDate), endAt(endDate));

        onValue(filterState, (snapshot) => {
            $("#table-report").DataTable().destroy();
            let html = "";
            let reports = [];
            snapshot.forEach((snapshotChild) => {
                let childKey = snapshotChild.key;
                let childData = snapshotChild.val();
                childData["firebase_id"] = snapshotChild.key;
                reports.push(childData);
                let currentDatetimeFormat = "YYYY-MM-DD HH:mm:ss";
                let validDatetimeFormat = "DD/MM/YYYY HH:mm";
                let dateApproveOperator = childData.date_approve_operator != 'out' ? moment(childData.date_approve_operator, currentDatetimeFormat).format(validDatetimeFormat) : 'out';
                let dateApproveMekanik = childData.date_approve_mekanik != 'out' ? moment(childData.date_approve_mekanik, currentDatetimeFormat).format(validDatetimeFormat) : 'out';
                let dateApproveForeman = childData.date_approve_foreman != 'out' ? moment(childData.date_approve_foreman, currentDatetimeFormat).format(validDatetimeFormat) : 'out';
                let dateApproveSuperintendent = childData.date_approve_superintendent != 'out' ? moment(childData.date_approve_superintendent, currentDatetimeFormat).format(validDatetimeFormat) : 'out';
                html += "<tr>\
                    <td class=\"align-middle text-center\">"+ childData.no_unit +"</td>\
                    <td class=\"align-middle text-nowrap\">\
                        <a href=\"{{ route('dashboard.p2h.index') }}/" + childKey + "\">" + childData.nomor_dokumen + "</a>\
                        <br>\
                        <p class=\"mb-0\">" + childData.status  + "</p>\
                    </td>\
                    <td class=\"align-middle text-center\">"+ dateApproveOperator +"</td>\
                    <td class=\"align-middle text-center\">"+ dateApproveMekanik +"</td>\
                    <td class=\"align-middle text-center\">"+ dateApproveForeman +"</td>\
                    <td class=\"align-middle text-center\">"+ dateApproveSuperintendent +"</td>\
                </tr>";
            });
            $(".input-report").text(JSON.stringify(reports));
            $("#tbody-report").html(html);
            $("#table-report").DataTable();
        })

        $(document).on("click", "#btn-download-pdf", function()
        {
            $("#btn-download-pdf-hidden").click();
            startLoading();
        });

        $(document).on("click", "#btn-download-excel", function()
        {
            $("#btn-download-excel-hidden").click();
        });

        $("#table-report").DataTable();
        $("a[href='{{ route('dashboard.report.index') }}']").closest(".nav-item").addClass("active");
    </script>
@endpush
