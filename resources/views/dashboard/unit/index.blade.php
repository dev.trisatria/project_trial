@extends('layouts.layout2')

@section('title', 'Unit')

@push('style')

@endpush

@section('content_header')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2 d-md-block d-none">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-left mb-0">Unit</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{ route('dashboard.index') }}">Beranda</a>
                            </li>
                            <li class="breadcrumb-item active">Unit</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-right col-md-3 col-12">
            <div class="form-group breadcrumb-right">
                <div class="dropdown">
                    @if (Auth::user()->role == 'superadmin')
                        <button class="btn btn-primary btn-round" type="button" id="btn-create">
                            Tambah
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-responsive d-block d-lg-table" id="table-unit">
                        <thead>
                            <tr>
                                <th style="width: 1%">ID</th>
                                <th>Tipe Unit</th>
                                <th>Nomor Unit</th>
                                <th>Merk Unit</th>
                                <th class="text-center" style="width: 1%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($list_unit as $unit)
                                <tr>
                                    <td>{{ $unit->id }}</td>
                                    <td>{{ $unit->tipe_unit }}</td>
                                    <td>{{ $unit->no_unit }}</td>
                                    <td>{{ $unit->merk_unit }}</td>
                                    <td>
                                        @if (Auth::user()->role == 'operator')
                                            <a href="{{ route('dashboard.p2h.create', ['unit_id' => $unit->id]) }}" class="btn btn-primary btn-round">P2H</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if (Auth::user()->role == 'superadmin')
        <div class="modal fade" id="modal-create">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Unit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('dashboard.unit.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Tipe Unit</label>
                                <input type="text" name="tipe_unit" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Nomor Unit</label>
                                <input type="text" name="no_unit" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Merk Unit</label>
                                <input type="text" name="merk_unit" class="form-control" required>
                            </div>
                            <div class="form-group mb-0 text-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@push('script')

    <script>
        $("#table-unit").DataTable();

        $(document).on("click", "#btn-create", function()
        {
            $("#modal-create").modal();
        });

        $("a[href='{{ route('dashboard.unit.index') }}']").closest(".nav-item").addClass("active");
    </script>
@endpush
