<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function ()
{
    return redirect()->route('login');
});

Route::middleware(['guest'])->group(function ()
{
    Route::get('forgot-password', 'ForgotPasswordController@showForgotPasswordForm');

    Route::post('forgot-password', 'ForgotPasswordController@sendMail')->name('forgot-password');

    Route::get('reset-password', 'ResetPasswordController@showResetPasswordForm');

    Route::post('reset-password', 'ResetPasswordController@reset')->name('password.reset');

    Route::get('login', 'AuthController@showLoginForm');

    Route::post('login', 'AuthController@authenticate')->name('login');
});

Route::middleware(['auth'])->group(function ()
{
    Route::post('logout', 'AuthController@logout')->name('logout');

    Route::get('dashboard', 'DashboardController')->name('dashboard.index');

    Route::name('dashboard.')->prefix('dashboard')->group(function()
    {
        Route::resource('unit', 'UnitController')->only('index', 'store');

        Route::prefix('p2h')->name('p2h.')->group(function()
        {
            Route::post('export-excel', 'P2HExportExcelController')->name('export-excel');

            Route::post('{firebaseId}/reminder', 'P2HReminderController')->name('reminder');

            Route::get('{firebaseId}/export-pdf', 'P2HExportPdfController')->name('export-pdf');

            Route::get('{firebaseId}/create-ticket', 'P2HCreateTicketController')->name('create-ticket');

            Route::get('{firebaseId}/export-ticket-pdf', 'P2HExportTicketPdfController')->name('export-ticket-pdf');
        });

        Route::resource('p2h', 'P2HController')->except('destroy');

        Route::prefix('report')->name('report.')->group(function()
        {
            Route::post('export-pdf', 'ReportExportPdfController')->name('export-pdf');

            Route::post('export-excel', 'ReportExportExcelController')->name('export-excel');
        });

        Route::resource('report', 'ReportController')->only('index');
    });
});
