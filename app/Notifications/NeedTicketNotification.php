<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NeedTicketNotification extends Notification
{
    use Queueable;

    public $p2h;
    public $isReminder;

    /**
     * Create a new notification instance.
     *
     * @param bool $isReminder
     * @return void
     */
    public function __construct($p2h, bool $isReminder)
    {
        $this->p2h = $p2h;
        $this->isReminder = $isReminder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = 'P2H - Need Ticket';
        if ($this->isReminder) {
            $subject = '[Reminder] ' . $subject;
        }
        return (new MailMessage)
                    ->subject($subject)
                    ->line('Dokumen P2H dengan nomor ' . $this->p2h->nomor_dokumen . ' membutuhkan pembuatan tiket dari foreman. Anda dapat melakukan pembuatan tiket dengan mengklik tombol dibawah.')
                    ->action('Create Ticket', route('dashboard.p2h.edit', $this->p2h->firebase_id))
                    ->line('Untuk dokumen tersebut mohon segera ditindaklanjuti, terima kasih.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
