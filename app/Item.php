<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'item';

    protected $guarded = [];

    public function p2h()
    {
        return $this->belongsTo(P2H::class, 'p2h_id', 'id');
    }

    public function list_pemeriksaan()
    {
        return $this->belongsTo(P2H::class, 'list_pemeriksaan_id', 'id');
    }
}
