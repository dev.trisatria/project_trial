<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListPemeriksaan extends Model
{
    protected $table = 'list_pemeriksaan';

    protected $guarded = ['id'];

    public function item()
    {
        return $this->hasMany(Item::class, 'list_pemeriksaan_id', 'id');
    }
}
