<?php

namespace App;

use App\Notifications\ResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user';

    protected $guarded = [
        'id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function operator_p2h()
    {
        return $this->hasMany(P2H::class, 'operator_id', 'id');
    }

    public function mekanik_p2h()
    {
        return $this->hasMany(P2H::class, 'mekanik_id', 'id');
    }

    public function foreman_p2h()
    {
        return $this->hasMany(P2H::class, 'foreman_id', 'id');
    }

    public function superintendent_p2h()
    {
        return $this->hasMany(P2H::class, 'superintendent_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
