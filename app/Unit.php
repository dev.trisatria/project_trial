<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'unit';

    protected $guarded = [];

    public function p2h()
    {
        return $this->hasMany(P2H::class, 'unit_id', 'id');
    }
}
