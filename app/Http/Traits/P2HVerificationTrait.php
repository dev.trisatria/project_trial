<?php

namespace App\Http\Traits;

use App\Notifications\NeedForemanVerificationNotification;
use App\Notifications\NeedMechanicVerificationNotification;
use App\Notifications\NeedSuperintendentVerificationNotification;
use App\Notifications\NeedTicketNotification;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

trait P2HVerificationTrait{
    protected function canVerify($p2h)
    {
        if (Auth::user()->role == 'mekanik' && $p2h->status == 'Need Mechanic Verification')
        {
            return true;
        }
        if (Auth::user()->role == 'foreman' && $p2h->status == 'Need Foreman Verification')
        {
            return true;
        }
        if (Auth::user()->role == 'foreman' && $p2h->status == 'Need Ticket')
        {
            return true;
        }
        if (Auth::user()->role == 'superintendent' && $p2h->status == 'Need Superintendent Verification')
        {
            return true;
        }

        return false;
    }

    protected function sendVerificationNotificaton($p2h, $status, $isReminder)
    {
        if ($status == 'Need Mechanic Verification')
        {
            $list_mekanik = User::where('role', 'mekanik')->get();
            if (count($list_mekanik) > 0) {
                Notification::send($list_mekanik, new NeedMechanicVerificationNotification($p2h, $isReminder));
            }
        }

        if ($status == 'Need Foreman Verification')
        {
            $list_foreman = User::where('role', 'foreman')->get();
            if (count($list_foreman) > 0) {
                Notification::send($list_foreman, new NeedForemanVerificationNotification($p2h, $isReminder));
            }
        }
        if ($status == 'Need Ticket')
        {
            $list_foreman = User::where('role', 'foreman')->get();
            if (count($list_foreman) > 0) {
                Notification::send($list_foreman, new NeedTicketNotification($p2h, $isReminder));
            }
        }
        if ($status == 'Need Superintendent Verification')
        {
            $list_superintendent = User::where('role', 'superintendent')->get();
            if (count($list_superintendent) > 0) {
                Notification::send($list_superintendent, new NeedSuperintendentVerificationNotification($p2h, $isReminder));
            }
        }
    }
}
