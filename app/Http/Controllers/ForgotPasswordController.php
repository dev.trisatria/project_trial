<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    public function showForgotPasswordForm()
    {
        return view('auth.forgot_password');
    }

    public function sendMail(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user == null)
        {
            return redirect()->back()->with('ERR', 'Email tidak terdaftar');
        }

        $credentials = $request->only('email');

        Password::sendResetLink($credentials);

        return redirect()->route('login')->with('OK', 'Berhasil mengirim tautan reset password ke email Anda');
    }
}
