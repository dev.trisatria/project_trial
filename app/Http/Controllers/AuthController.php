<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->remember_me ?? false)) {
            return redirect()->intended('dashboard');
        }

        return back()->with('ERR', 'Email atau password salah');
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('login');
    }
}
