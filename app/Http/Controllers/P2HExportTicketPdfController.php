<?php

namespace App\Http\Controllers;

use App\Item;
use App\P2H;
use Barryvdh\DomPDF\Facade\Pdf;

class P2HExportTicketPdfController extends Controller
{
    public function __invoke($firebaseId)
    {
        return $this->export($firebaseId);
    }

    public function export($firebaseId)
    {
        $data['p2h'] = P2H::with('unit','mekanik', 'foreman')->where('firebase_id', $firebaseId)->firstOrFail();
        $data['list_pemeriksaan_rusak'] = Item::join('list_pemeriksaan', 'list_pemeriksaan.id', '=', 'item.list_pemeriksaan_id')->select('item.*', 'list_pemeriksaan.nama_pemeriksaan', 'list_pemeriksaan.kode_bahaya', 'list_pemeriksaan.kategori')->where('p2h_id', $data['p2h']->id)->where('status_checklist', 'Rusak/Tidak Normal')->get();

        $pdf = Pdf::loadView('exports.p2h.ticket.pdf', $data);
        return $pdf->stream();
    }
}
