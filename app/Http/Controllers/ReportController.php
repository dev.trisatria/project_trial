<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        $startDate = $request->start_date ?? date('Y-m-01');
        $endDate = $request->end_date ?? date('Y-m-t');
        $data['start_date'] = $startDate . ' 00:00:00';
        $data['end_date'] = $endDate . ' 23:59:59';

        return view('dashboard.report.index', $data);
    }
}
