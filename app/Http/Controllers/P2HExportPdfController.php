<?php

namespace App\Http\Controllers;

use App\Item;
use App\P2H;
use Barryvdh\DomPDF\Facade\Pdf;

class P2HExportPdfController extends Controller
{
    public function __invoke($firebaseId)
    {
        return $this->export($firebaseId);
    }

    public function export($firebaseId)
    {
        $data['p2h'] = P2H::with('unit', 'operator', 'mekanik', 'foreman', 'superintendent')->where('firebase_id', $firebaseId)->firstOrFail();
        $data['list_kategori'] = ['Pemeriksaan Keliling Unit/Diluar Kabin', 'Pemeriksaan Didalam Kabin', 'Pemeriksaan Di Ruang Mesin'];
        $data['list_pemeriksaan'] = Item::join('list_pemeriksaan', 'list_pemeriksaan.id', '=', 'item.list_pemeriksaan_id')->where('p2h_id', $data['p2h']->id)->get()->groupBy('kategori');

        $pdf = Pdf::loadView('exports.p2h.pdf', $data);
        return $pdf->stream();
    }
}
