<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    public function showResetPasswordForm(Request $request)
    {
        return view('auth.reset_password');
    }

    public function reset(Request $request)
    {
        $credentials = $request->only('email', 'password', 'token');

        $reset_password_status = Password::reset($credentials, function ($user, $password)
        {
            $user->update([
                'password' => bcrypt($password)
            ]);
        });

        if ($reset_password_status == Password::INVALID_TOKEN)
        {
            return redirect()->back()->with("ERR", "Token tidak valid");
        }

        return redirect()->route('login')->with("OK", "Password berhasil diperbarui");
    }
}
