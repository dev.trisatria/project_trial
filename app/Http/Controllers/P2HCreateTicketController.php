<?php

namespace App\Http\Controllers;

use App\Http\Traits\P2HVerificationTrait;
use App\Item;
use App\P2H;

class P2HCreateTicketController extends Controller
{
    use P2HVerificationTrait;

    public function __invoke($firebaseId)
    {
        return $this->createTicket($firebaseId);
    }

    public function createTicket($firebaseId)
    {
        $data['p2h'] = P2H::with('unit','mekanik', 'foreman')->where('firebase_id', $firebaseId)->firstOrFail();

        if (!$this->canVerify($data['p2h'])) {
            return redirect()->intended('dashboard/p2h')->with('ERR', 'Tidak dapat melakukan aksi tersebut');
        }

        $data['list_kategori'] = ['Pemeriksaan Keliling Unit/Diluar Kabin', 'Pemeriksaan Didalam Kabin', 'Pemeriksaan Di Ruang Mesin'];
        $data['list_pemeriksaan'] = Item::join('list_pemeriksaan', 'list_pemeriksaan.id', '=', 'item.list_pemeriksaan_id')->select('item.*', 'list_pemeriksaan.nama_pemeriksaan', 'list_pemeriksaan.kode_bahaya', 'list_pemeriksaan.kategori')->where('p2h_id', $data['p2h']->id)->get()->groupBy('kategori');
        $data['list_pemeriksaan_rusak'] = Item::join('list_pemeriksaan', 'list_pemeriksaan.id', '=', 'item.list_pemeriksaan_id')->select('item.*', 'list_pemeriksaan.nama_pemeriksaan', 'list_pemeriksaan.kode_bahaya', 'list_pemeriksaan.kategori')->where('p2h_id', $data['p2h']->id)->where('status_checklist', 'Rusak/Tidak Normal')->get();

        return view('dashboard.p2h.ticket.create', $data);
    }
}
