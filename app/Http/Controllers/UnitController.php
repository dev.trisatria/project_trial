<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    public function index()
    {
        $data['list_unit'] = Unit::all();

        return view('dashboard.unit.index', $data);
    }

    public function store(Request $request)
    {
        $dataUnit = [
            'tipe_unit' => $request->tipe_unit,
            'no_unit' => $request->no_unit,
            'merk_unit' => $request->merk_unit,
        ];
        Unit::create($dataUnit);

        return redirect()->back()->with('OK', 'Data berhasil ditambahkan');
    }
}
