<?php

namespace App\Http\Controllers;

use App\Http\Traits\P2HVerificationTrait;
use App\Item;
use App\ListPemeriksaan;
use App\P2H;
use App\Report;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class P2HController extends Controller
{
    use P2HVerificationTrait;

    public function index(Request $request)
    {
        $startDate = $request->start_date ?? date('Y-m-01');
        $endDate = $request->end_date ?? date('Y-m-t');
        $data['start_date'] = $startDate . ' 00:00:00';
        $data['end_date'] = $endDate . ' 23:59:59';

        $data['list_p2h'] = P2H::with('unit')->where('jam', '>=', $data['start_date'])->where('jam', '<=', $data['end_date'])->orderByDesc('jam')->get();

        return view('dashboard.p2h.index', $data);
    }

    public function create(Request $request)
    {
        $data['unit'] = Unit::findOrFail($request->unit_id);
        $data['list_kategori'] = ['Pemeriksaan Keliling Unit/Diluar Kabin', 'Pemeriksaan Didalam Kabin', 'Pemeriksaan Di Ruang Mesin'];
        $data['list_pemeriksaan'] = ListPemeriksaan::get()->groupBy('kategori');

        return view('dashboard.p2h.create', $data);
    }

    public function store(Request $request)
    {
        $dataP2H = [
            'firebase_id' => $request->firebase_id,
            'unit_id' => $request->unit_id,
            'nomor_dokumen' => $request->nomor_dokumen,
            'jam' => date('Y-m-d H:i:s', strtotime($request->jam)),
            'hm_km' => $request->hm_km,
            'operator_id' => Auth::user()->id,
            'status' => 'Need Mechanic Verification',
            'created_at' => $request->current_time,
            'updated_at' => $request->current_time,
        ];
        $p2h = P2H::create($dataP2H);

        $dataListItem = $request->list_item;
        for ($i = 0; $i < count($dataListItem); $i++) {
            $item = $dataListItem[$i];
            $item['p2h_id'] = $p2h->id;
            $item['created_at'] = $request->current_time;
            $item['updated_at'] = $request->current_time;
            $dataListItem[$i] = $item;
        }
        Item::insert($dataListItem);

        $dataReport = [
            'p2h_id' => $p2h->id,
            'date_approve_operator' => $request->current_time,
            'status' => $p2h->status,
            'created_at' => $request->current_time,
            'updated_at' => $request->current_time,
        ];
        Report::create($dataReport);

        $this->sendVerificationNotificaton($p2h, $p2h->status, false);

        return redirect()->route('dashboard.p2h.index')->with('OK', 'Data berhasil disimpan');
    }

    public function show($firebaseId)
    {
        $data['p2h'] = P2H::with('unit', 'operator', 'mekanik', 'foreman', 'superintendent')->where('firebase_id', $firebaseId)->firstOrFail();
        $data['list_kategori'] = ['Pemeriksaan Keliling Unit/Diluar Kabin', 'Pemeriksaan Didalam Kabin', 'Pemeriksaan Di Ruang Mesin'];
        $data['list_pemeriksaan'] = Item::join('list_pemeriksaan', 'list_pemeriksaan.id', '=', 'item.list_pemeriksaan_id')->where('p2h_id', $data['p2h']->id)->get()->groupBy('kategori');

        return view('dashboard.p2h.show', $data);
    }

    public function edit($firebaseId)
    {

        $data['p2h'] = P2H::with('unit', 'operator', 'mekanik', 'foreman', 'superintendent')->where('firebase_id', $firebaseId)->firstOrFail();

        if (!$this->canVerify($data['p2h'])) {
            return redirect()->intended('dashboard/p2h')->with('ERR', 'Tidak dapat melakukan aksi tersebut');
        }

        $data['list_kategori'] = ['Pemeriksaan Keliling Unit/Diluar Kabin', 'Pemeriksaan Didalam Kabin', 'Pemeriksaan Di Ruang Mesin'];
        $data['list_pemeriksaan'] = Item::join('list_pemeriksaan', 'list_pemeriksaan.id', '=', 'item.list_pemeriksaan_id')->select('item.*', 'list_pemeriksaan.nama_pemeriksaan', 'list_pemeriksaan.kode_bahaya', 'list_pemeriksaan.kategori')->where('p2h_id', $data['p2h']->id)->get()->groupBy('kategori');

        return view('dashboard.p2h.edit', $data);
    }

    public function update(Request $request, $firebaseId)
    {
        $p2h = P2H::with('unit', 'report', 'operator', 'mekanik', 'foreman', 'superintendent')->where('firebase_id', $firebaseId)->firstOrFail();
        $p2h->update([
            'mekanik_id' => $request->mekanik_id ?? $p2h->mekanik_id,
            'foreman_id' => $request->foreman_id ?? $p2h->foreman_id,
            'superintendent_id' => $request->superintendent_id ?? $p2h->superintendent_id,
            'catatan' => $request->catatan,
            'status' => $request->status
        ]);

        Item::where('p2h_id', $p2h->id)->delete();
        Item::insert($request->list_item);

        $p2h->report->update([
            'date_approve_mekanik' => $request->mekanik_id != null ? $request->current_time : $p2h->report->date_approve_mekanik,
            'date_approve_foreman' => $request->foreman_id != null ? $request->current_time : $p2h->report->date_approve_foreman,
            'date_approve_superintendent' => $request->superintendent_id != null ? $request->current_time : $p2h->report->date_approve_superintendent,
            'status' => $request->status,
            'updated_at' => $request->current_time
        ]);

        $this->sendVerificationNotificaton($p2h, $request->status, false);

        return redirect()->route('dashboard.p2h.index')->with('OK', 'Data berhasil diperbarui');
    }
}
