<?php

namespace App\Http\Controllers;

use App\Http\Traits\P2HVerificationTrait;
use App\P2H;

class P2HReminderController extends Controller
{
    use P2HVerificationTrait;

    public function __invoke($firebaseId)
    {
        return $this->sendReminderNotification($firebaseId);
    }

    public function sendReminderNotification($firebaseId)
    {
        $p2h = P2H::where('firebase_id', $firebaseId)->firstOrFail();
        $this->sendVerificationNotificaton($p2h, $p2h->status, true);

        return redirect()->back()->with('OK', 'Reminder berhasil dikirim');
    }
}
