<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class ReportExportPdfController extends Controller
{
    public function __invoke(Request $request)
    {
        return $this->export($request);
    }

    public function export(Request $request)
    {
        $data['list_report'] = json_decode($request->list_report);
        $pdf = Pdf::loadView('exports.report.pdf', $data);
        return $pdf->stream();
    }
}
