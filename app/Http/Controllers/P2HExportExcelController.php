<?php

namespace App\Http\Controllers;

use App\Exports\P2HExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class P2HExportExcelController extends Controller
{
    public function __invoke(Request $request)
    {
        return $this->export($request);
    }

    public function export(Request $request)
    {
        $data['list_p2h'] = json_decode($request->list_p2h);

        return Excel::download(new P2HExport($data), 'p2h.xlsx');
    }
}
