<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class P2H extends Model
{
    protected $table = 'p2h';

    protected $guarded = ['id'];

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id', 'id');
    }

    public function operator()
    {
        return $this->belongsTo(User::class, 'operator_id', 'id');
    }

    public function mekanik()
    {
        return $this->belongsTo(User::class, 'mekanik_id', 'id');
    }

    public function foreman()
    {
        return $this->belongsTo(User::class, 'foreman_id', 'id');
    }

    public function superintendent()
    {
        return $this->belongsTo(User::class, 'superintendent_id', 'id');
    }

    public function report()
    {
        return $this->hasOne(Report::class, 'p2h_id', 'id');
    }

    public function item()
    {
        return $this->hasMany(Item::class, 'p2h_id', 'id');
    }
}
