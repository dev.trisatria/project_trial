<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'report';

    protected $guarded = ['id'];

    public function p2h()
    {
        return $this->belongsTo(P2H::class, 'p2h_id', 'id');
    }
}
