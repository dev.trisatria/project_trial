<?php

use App\ListPemeriksaan;
use App\Unit;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $userDefault = User::all('id');
        if (count($userDefault) == 0)
        {
            $listRole = ['operator', 'mekanik', 'foreman', 'superintendent', 'superadmin'];
            $user = [];
            foreach ($listRole as $key => $role)
            {
                $user[] = [
                    'nama' => ucfirst($role),
                    'nip' => $key . rand(1000000, 9999999) .'XXXXX',
                    'role' => $role,
                    'email' => $role . '@trisatria.dev',
                    'password' => bcrypt('password'),
                    'tanda_tangan' => '/storage/tanda_tangan/ttd-example.png',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }
            $user[] = [
                'nama' => 'Operator Trial',
                'nip' => '4' . rand(1000000, 9999999) .'XXXXX',
                'role' => 'operator',
                'email' => 'trisatria.trial@gmail.com',
                'password' => bcrypt('password'),
                'tanda_tangan' => '/storage/tanda_tangan/ttd-example.png',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            User::insert($user);
        }
        $listKategoriPemeriksaan = [
            (object)[
                'nama_kategori' => 'Pemeriksaan Keliling Unit/Diluar Kabin',
                'list_pemeriksaan' => [
                    (object)["nama_pemeriksaan" => "Ban & baut roda", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Kerusakaan akibat insiden***)", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Kebocoran Oli transmisi", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Semua Oli Hidrolik", "kode_bahaya" => "A"],
                    (object)["nama_pemeriksaan" => "Fuel Drain/Buangan air dari tangki BBC", "kode_bahaya" => "A"],
                    (object)["nama_pemeriksaan" => "BBC Minimum 25% dari Cap. Tangki", "kode_bahaya" => "A"],
                    (object)["nama_pemeriksaan" => "Buang air dalam tangki udara", "kode_bahaya" => "A"],
                    (object)["nama_pemeriksaan" => "Level Oli Steering", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Level Oli Rem", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Level Oli Kopling", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Kebersihan accessories safety", "kode_bahaya" => "A"],
                    (object)["nama_pemeriksaan" => "Ganjal 2", "kode_bahaya" => "A"],
                    (object)["nama_pemeriksaan" => "Safety Cone 2 (disimpan dalam cabin)", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Kebocoran Kran & Selang", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Kebocoran Tangki", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Fire Extinguisher / APAR 2x 6 Kg", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Kebocoran Knalpot", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Alarm Mundur", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Kelainan saat operasi", "kode_bahaya" => "AA"],
                ]
            ],
            (object)[
                'nama_kategori' => 'Pemeriksaan Didalam Kabin',
                'list_pemeriksaan' => [
                    (object)["nama_pemeriksaan" => "Fungsi brake/ Fungsi Rem", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Fungsi steering/Kemudi", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Fungsi seat belt", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Semua Lampu/ Rotary", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Fungsi mirror ( spion )", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Wiper dan level Air Wiper", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Klakson", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Kontrol panel", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Fungsi Radio Komunikasi", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Kebersihan ruang Kabin", "kode_bahaya" => "A"],
                    (object)["nama_pemeriksaan" => "In Car Camera", "kode_bahaya" => "AA"],
                ]
            ],
            (object)[
                'nama_kategori' => 'Pemeriksaan Di Ruang Mesin',
                'list_pemeriksaan' => [
                    (object)["nama_pemeriksaan" => "Air radiator", "kode_bahaya" => "AA"],
                    (object)["nama_pemeriksaan" => "Oil Engine/Oli Mesin", "kode_bahaya" => "AA"],
                ]
            ]
        ];

        $listPemeriksaanDefault = ListPemeriksaan::all('id');
        if (count($listPemeriksaanDefault) == 0)
        {
            $pemeriksaan = [];
            foreach ($listKategoriPemeriksaan as $key => $listKategori)
            {
                foreach ($listKategori->list_pemeriksaan as $key => $listPemeriksaan)
                {
                    $pemeriksaan[] = [
                        'kategori' => $listKategori->nama_kategori,
                        'nama_pemeriksaan' => $listPemeriksaan->nama_pemeriksaan,
                        'kode_bahaya' => $listPemeriksaan->kode_bahaya,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                }
            }
            ListPemeriksaan::insert($pemeriksaan);
        }

        Unit::create([
            'tipe_unit' => 'Tipe unit A',
            'no_unit' => '07123181239',
            'merk_unit' => 'Merk xyz',
        ]);

        // ListPemeriksaan
    }
}
