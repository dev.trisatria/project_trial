<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateP2HTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p2h', function (Blueprint $table) {
            $table->id();
            $table->foreignId('unit_id');
            $table->bigInteger('firebase_id')->nullable();
            $table->string('nomor_dokumen');
            $table->timestamp('jam');
            $table->string('hm_km');
            $table->foreignId('operator_id')->nullable();
            $table->foreignId('mekanik_id')->nullable();
            $table->foreignId('foreman_id')->nullable();
            $table->foreignId('superintendent_id')->nullable();
            $table->enum('status', ['Need Mechanic Verification', 'Need Foreman Verification', 'Need Ticket', 'Ticket Created', 'Need Superintendent Verification', 'Completed']);
            $table->timestamps();

            $table->foreign('unit_id')->references('id')->on('unit')->onDelete('cascade');
            $table->foreign('operator_id')->references('id')->on('user')->onDelete('set null');
            $table->foreign('mekanik_id')->references('id')->on('user')->onDelete('set null');
            $table->foreign('foreman_id')->references('id')->on('user')->onDelete('set null');
            $table->foreign('superintendent_id')->references('id')->on('user')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p2h');
    }
}
