<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->id();
            $table->foreignId('p2h_id');
            $table->foreignId('list_pemeriksaan_id');
            $table->enum('status_checklist', ['Baik/Normal', 'Rusak/Tidak Normal']);
            $table->text('catatan')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();

            $table->foreign('p2h_id')->references('id')->on('p2h')->onDelete('cascade');
            $table->foreign('list_pemeriksaan_id')->references('id')->on('list_pemeriksaan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
