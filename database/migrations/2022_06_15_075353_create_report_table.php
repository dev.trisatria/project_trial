<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report', function (Blueprint $table) {
            $table->id();
            $table->foreignId('p2h_id');
            $table->timestamp('date_approve_operator')->nullable();
            $table->timestamp('date_approve_mekanik')->nullable();
            $table->timestamp('date_approve_foreman')->nullable();
            $table->timestamp('date_approve_superintendent')->nullable();
            $table->enum('status', ['Need Mechanic Verification', 'Need Foreman Verification', 'Need Ticket', 'Ticket Created', 'Need Superintendent Verification', 'Completed']);
            $table->timestamps();

            $table->foreign('p2h_id')->references('id')->on('p2h')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report');
    }
}
