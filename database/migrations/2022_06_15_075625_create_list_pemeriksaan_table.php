<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListPemeriksaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_pemeriksaan', function (Blueprint $table) {
            $table->id();
            $table->string('nama_pemeriksaan');
            $table->enum('kategori', ['Pemeriksaan Keliling Unit/Diluar Kabin', 'Pemeriksaan Didalam Kabin', 'Pemeriksaan Di Ruang Mesin']);
            $table->string('kode_bahaya');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_pemeriksaan');
    }
}
